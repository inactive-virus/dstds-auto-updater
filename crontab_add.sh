#!/bin/bash
# Author: inactive_virus <inactive0v0@magireco.moe>
# Source: https://gitlab.com/inactive_virus/auto_update_dst

CRON="0 * * * * $PWD/auto_update_dst.sh"
[ "$1" == "eng" ] && CRON="0 * * * * $PWD/auto_update_dst_eng.sh"
crontab -l > crontab.now

if cat crontab.now | grep "auto_update_dst"; then
 echo '计划任务已经存在. | Already Exist.' 
else
 echo "$CRON &>> auto_update_dst.log" >> crontab.now
 crontab crontab.now && echo "已添加到crontab. | Added to crontab."
fi

rm -f crontab.now && echo "移除临时文件. | Removeds temporary file."
