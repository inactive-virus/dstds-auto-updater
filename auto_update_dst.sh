#!/bin/bash
# Author: inactive_virus <inactive0v0@magireco.moe>
# Source: https://gitlab.com/inactive_virus/auto_update_dst

# 使用crontab每小时运行脚本
# 可使用 crontab_add.sh 或 crontab -e 手动添加
# 0 * * * * 脚本路径/auto_update_dst.sh &> auto_update_dst.log

# 变量配置
STEAMCMD_DIR="$HOME/steamcmd"
# Steamcmd的安装目录
INSTALL_DIR="$HOME/dst_dedicated"
# 饥荒服务端的安装目录
SCREEN_NAME="master"
# 运行饥荒服务器脚本的screen名
START_SCRIPT="$HOME/run_shard.sh"
# 运行饥荒服务器的脚本路径
SERVER_LOG="$HOME/.klei/DoNotStarveTogether/WithLoveWitly/Master/server_log.txt"
# 服务器日志文件的路径

function error()
{
	echo 错误: "$@" >&2
	exit 1
}

function check()
{
	[ -e "$1" ] || error "丢失文件 $1"
	[ -r "$1" ] || error "无法读取 $1"
}

function restart()
{
	screen -S "$SCREEN_NAME" -p - -X stuff "^C"
	#如果服务端没运行，c_shutdown()会被当成函数声明，导致之后都不能执行
	#所以采用了发送^C来结束服务端，其效果等同c_shutdown()
	#手动这样做会掐断sed，导致屏幕上看不到之后的输出
	for i in {1..10}; do
	sleep 10
	tail -n 1 $SERVER_LOG | grep 'Shutting down' && break
	[ $i = "10" ] && error '服务器未正常关闭'
	#此处做10次尝试，每等待10秒后调取日志确认服务器是否成功关闭
	#有结果时会输出并跳出循环，否则给出错误
	done
	screen -S "$SCREEN_NAME" -p - -X stuff "bash $START_SCRIPT\r" && echo "服务器启动"
}
    
# 主程序
	# 报时
	date +"%Y-%m-%d %T"
    
	# 检查所需文件
	check "$STEAMCMD_DIR/steamcmd.sh"
	check "$INSTALL_DIR/version.txt"
	# 这个文件和脚本运行无关，只是防止弄错安装路径导致花很长时间去下载
	check "$SERVER_LOG"
	check "$START_SCRIPT"
    
    # 尝试更新
	for i in {1..11}; do
		bash $STEAMCMD_DIR/steamcmd.sh +force_install_dir "$INSTALL_DIR" +login anonymous +app_update 343050 +quit > steamcmd.log
		##嘤为前面并没有检查steamcmd.sh的x权限，咕采用只需要r权限的bash
		cat steamcmd.log | grep 'Success!' && break 
		#已知有时在Success!行之后还会有discard的输出，所以此处不使用tail
		#网络问题时出现的是Error!
		[ $i = "11" ] && error '10次更新失败，取消操作'
	done
	
    # 全部成功后判断是否重启服务端
	cat steamcmd.log | grep  'fully installed' >>/dev/null && restart || echo '无需重启'
