#!/bin/bash
# Author: inactive_virus(inactive0v0@magireco.moe)
# Source: https://gitlab.com/inactive_virus/auto_update_dst

# 变量配置
STEAMCMD_DIR="$HOME/steamcmd"
# Steamcmd的安装目录
INSTALL_DIR="$HOME/dst_dedicated"
# 饥荒服务端的安装目录
SERVER_DIR="$HOME/.klei/DoNotStarveTogether"
# 服务器配置文件的主目录
# 没有用-persistent_storage_root时无需修改

# 单个的服务器称为 'SHARD'(成员)
# 相互连接起来之后共同构成一个 'CLUSTER'(群组)
# 最普遍的配置就是由地上、地下两个SHARD构成一个CLUSTER
# 网上流行的脚本，其群组名多为MyDediServer
# 建议自定义CLUSTER_NAME
# 如果在同一台服务器上跑地上和地下
# 请复制一份此脚本然后修改SHARD_NAME
# 按惯例，地上为Master 地下为Caves
CLUSTER_NAME="WithLoveWitly"
# 服务器群组的名称
SHARD_NAME="Master"
# 服务器成员的名称

PREFIX="$SHARD_NAME@$CLUSTER_NAME "
# 控制台输出信息的显示前缀，方便区分
# 推荐管理多个服务器群组时用默认值
# 管理单个服务器群组时用 "$SHARD_NAME "即可

function error()
{
	echo 错误: "$@" >&2
	exit 1
}

function check()
{
	[ -e "$1" ] || error "丢失文件 $1"
	[ -r "$1" ] || error "无法读取 $1"
}


check "$SERVER_DIR/$CLUSTER_NAME/cluster.ini"
check "$SERVER_DIR/$CLUSTER_NAME/cluster_token.txt"
check "$SERVER_DIR/$CLUSTER_NAME/$SHARD_NAME/server.ini"
check "$INSTALL_DIR/bin/dontstarve_dedicated_server_nullrenderer"

cd "$INSTALL_DIR/bin" || error "无法进入服务端目录"
[ -x ./dontstarve_dedicated_server_nullrenderer ] || error "没有服务端执行权限"
# 很迷的是，必须把工作路径拉到bin来，否则后面../data之类的路径都无法读取
# 否则我会用绝对路径

if [ -z "$PREFIX" ] ; then
	./dontstarve_dedicated_server_nullrenderer \
	-cluster "$CLUSTER_NAME" \
	-shard "$SHARD_NAME" 
else
	./dontstarve_dedicated_server_nullrenderer \
	-cluster "$CLUSTER_NAME" \
	-shard "$SHARD_NAME" \
	| sed "s/^/$PREFIX /g"
fi
# -console参数已经不推荐使用 
# 改为在server.ini中用 console_enabled 来配置
# -monitor_parent_process $$ 查无此参数，移除
